<?php

namespace App\Helpers;

class InstagramPhotos
{
	 /**
     * Guzzle Http client instance
     *
     * @var GuzzleHttp\Client
     */
	private static $client = "";

	/**
     * Access Token
     *
     * @var string
     */
	private static $access_token = "";

	/**
     * User id
     *
     * @var integer
     */
	private static $user_id = 0;

	/**
     * Set client, access token and user id
     *
     * @param GuzzleHttp\Client $client
     * @param Illuminate\Http\Request 
     * @return App\Helpers\InstagramPhotos self
     */
	public static function configure($client, $request)
	{
		self::$client = $client;
		if ($request->session()->get('instagram_data')) {
			$access_token_data = json_decode($request->session()->get('instagram_data'));
			self::$user_id = $access_token_data->user_id;
			self::$access_token = $access_token_data->access_token;
		} else {
			$token_data = self::$client->request('POST', 'https://api.instagram.com/oauth/access_token', [
	        	'form_params' => [
				    'app_id' => config('app.instagram_app_id'), 
				    'app_secret' => config('app.instagram_secret'),
				    'grant_type' => 'authorization_code',
				    'redirect_uri' => config('app.instagram_redirect_uri'),
				    'code' => $request->code
				]
			]);
			$data = json_decode($token_data->getBody()->getContents());
			if (empty($data->access_token)) {
				return [
	                'success' => false,
	                'errors' => $token_data->getBody()->getContents()
	            ];
			}
			self::$user_id = $data->user_id;
			self::$access_token = $data->access_token;
			$request->session()->put('instagram_data', json_encode([
				'user_id' => $data->user_id,
				'access_token' => $data->access_token
			]));
		}
		return new static;
	}

    /**
     * Get photos from Instagram
     * @return array
     */
    public static function getPhotos()
    {
        try {
    		$photos = self::$client->request('GET', 'https://graph.instagram.com/' . self::$user_id . '/media', [
    			'query' => [
    			    'fields' => 'media_url', 
    			    'access_token' => self::$access_token
    			]
    		]);
        } catch (\Exception $e) {
            $token_expired = self::checkForTokenExpiration($e->getMessage());
            if ($token_expired) {
                return [
                    'success' => false,
                    'errors' => 'token_expired'
                ];
            }
        }
		$photos = json_decode($photos->getBody()->getContents());
		if (empty($photos->data)) {
			return [
                'success' => false,
                'errors' => $photos->getBody()->getContents()
            ];
		}
        return [
            'success' => true,
            'photos' => $photos->data,
            'paging' => isset($photos->paging->next) ? $photos->paging->next : 0  
        ];
    }

    /**
     * Get photos by "next" token from Instagram
     * @param string base64 encoded $code
     * @return array
     */
    public static function getMorePhotos($code)
    {
    	$code = base64_decode($code);
        if (preg_match('/^https:\/\/graph\.instagram\.com/i', $code) == false) {
            return [
                'success' => false,
                'errors' => 'invalid_url'
            ];
        }
        try {
            $photos = self::$client->request('GET', $code);
        } catch (\Exception $e) {
            $token_expired = self::checkForTokenExpiration($e->getMessage());
            if ($token_expired) {
                return [
                    'success' => false,
                    'errors' => 'token_expired'
                ];
            }
        }
		$photos = json_decode($photos->getBody()->getContents());
		if (empty($photos->data)) {
			return [
                'success' => false,
                'errors' => $photos->getBody()->getContents()
            ];
		}
        return [
            'success' => true,
            'photos' => $photos->data,
            'paging' => isset($photos->paging->next) ? $photos->paging->next : 0  
        ];
    }

    /**
     * Get photos from current post
     * @param integer $id
     * @return array
     */
    public static function getPostPhotos($id)
    {
    	try {
            $photos = self::$client->request('GET', 'https://graph.instagram.com/' . $id . '/children', [
    			'query' => [
    			    'fields' => 'media_url', 
    			    'access_token' => self::$access_token
    			]
    		]);
        } catch (\Exception $e) {
            $token_expired = self::checkForTokenExpiration($e->getMessage());
            if ($token_expired) {
                return [
                    'success' => false,
                    'errors' => 'token_expired'
                ];
            }
        }
		$photos = json_decode($photos->getBody()->getContents());
		if (empty($photos->data)) {
			return [
                'success' => false
            ];
		}
        return [
            'success' => true,
            'photos' => $photos->data
        ];
    }

    /**
     * Check if token expired
     * @param string $message
     * @return boolean
     */
    protected static function checkForTokenExpiration($message)
    {
        if (strpos($message, 'expired') !== false) {
            request()->session()->forget('instagram_data');
            return true;
        }
        return false;
    }

}